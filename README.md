# priest

 ___Here is the priest...___

___There is the priest...___

___Where is the Priest?___



Card hustlers have always existed, in every corner of the globe: Ancient Egypt, Ancient Greece, Roman Times...


This game was used by many of them to get the innocent passer's-by money.


In modern Greece this game is called ___'The Priest' (Ο παπάς)___,
but the names from around the world and across the Ages, are innumerable :(_Three-card Monte, Bonneteau, Gioco delle tre Carte, Kümmelblättchen, Wakaleba, Bul Karayı Al Parayı, bola bola,_ to name but a few).


In the Greek language, the characterisation ___'papatzis'___ is adhered to any person who posseses the gift of gab, and is prone to lies, fraud and mischief.


However, noone is scamming anyone in this computer version of the game.

![00.png](png/00.png)



## INSTRUCTIONS

* Download zip file pressing the download button above.

* Unzip

* Double-click on index.html file, to open the game in a browser tab.



---
## LET'S WIN SOME EASY MONEY! (or get poor tryin')

As soon as you run the program, you will be prompted to select the type of  game.

You can select either

1. a game of ten hands (amount as much money as possible in 10 hands)

2. to **Go Deep** (no hand limit, you can play until you lose all your money)

![0.png](png/0.png)


* The player can place a bet by either clicking on the slider, or mouse-scrolling on any card.

![1.png](png/1.png)

* After 10 hands (or if you **Go Deep**, when you run out of money, you will be shown your performance, and asked if you would like to play again.

![2.png](png/2.png)



---

## Settings

*   By clicking on on the  __Settings  button__  🛠, the player can set the values and the appearence of the game according to their liking:

![preferences.png](png/preferences.png)

The user can set

* the background color
* the deck of cards
* the back image of the deck
* Key that hides / shows again the game (boss key 😉 )
* The initial sum
* The currency ($,€,￥,￡ etc)
* The currency format (coin symbol before or after the amount)
* Show Trophies On / Off
* Sound On / Off
* Animation On / Off
* Save / Load settings in / to a file, for easy & quick access from the file system.

Clicking on the __Done__ button will bring the user back to the game.


---

When you win a bold bet , or a big amount, the game rewards you with a trophy!


---
__Good Luck, you are going to need it!__


**ENJOY!**
