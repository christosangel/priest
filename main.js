//
//▗▄▄▖        █
//▐▛▀▜▖       ▀             ▐▌
//▐▌ ▐▌ █▟█▌ ██   ▟█▙ ▗▟██▖▐███
//▐██▛  █▘    █  ▐▙▄▟▌▐▙▄▖▘ ▐▌
//▐▌    █     █  ▐▛▀▀▘ ▀▀█▖ ▐▌
//▐▌    █   ▗▄█▄▖▝█▄▄▌▐▄▄▟▌ ▐▙▄
//▝▘    ▀   ▝▀▀▀▘ ▝▀▀  ▀▀▀   ▀▀
//
//
//a solitaire card game written by Christos Angelopoulos, under GPL v2, Dec 2022
function createCards()
{
  var CARD0 = document.createElement("img");
  CARD0.id='CARD0';
  document.getElementById("cards").appendChild(CARD0);
  var CARD1 = document.createElement("img");
  CARD1.id='CARD1';
  document.getElementById("cards").appendChild(CARD1);
  var CARD2 = document.createElement("img");
  CARD2.id='CARD2';
  document.getElementById("cards").appendChild(CARD2);
  cardsClosed();
}
function reloadPreferedValues()
{
  document.body.style.background = document.querySelector("#prefBGColor").textContent;
  document.querySelector("#colorPick").value = document.querySelector("#prefBGColor").textContent;
  document.querySelector("#bossHide").style.background = document.querySelector("#prefBGColor").textContent;
  document.querySelector("#inSumInp").style.background = document.querySelector("#prefBGColor").textContent;
  document.querySelector("#currencySelect").style.background = document.querySelector("#prefBGColor").textContent;
  document.querySelector("#currencyFormat").style.background = document.querySelector("#prefBGColor").textContent;
  document.querySelector("#trophySp").style.background = document.querySelector("#prefBGColor").textContent;
  document.querySelector("#audioSp").style.background = document.querySelector("#prefBGColor").textContent;
  document.querySelector("#inSumInp").value = document.querySelector("#prefInSum").textContent;
  document.querySelector("#bossHide").value = document.querySelector("#prefBoss").textContent;
  document.querySelector("#currencySelect").value  =  document.querySelector("#prefCurrency").textContent;
  document.querySelector("#currencyFormat").value = document.querySelector("#prefCurrencyFormat").textContent;
  if ( document.querySelector('#CARD2').src.replace(/.*png\//, '').replace(/\/.*/, '') !== "back" ){
    document.querySelector('#CARD2').src = document.querySelector('#CARD2').src.replace(/.*png\/./, document.querySelector("#prefSet").textContent)
  }else {
    document.querySelector('#CARD2').src = document.querySelector("#prefDeck").textContent;
  };
  if ( String(document.querySelector('#CARD0').src.match('back')) !== "back" ){
    document.querySelector('#CARD0').src = document.querySelector('#CARD0').src.replace(/.*png\/./, document.querySelector("#prefSet").textContent)
    document.querySelector('#CARD1').src = document.querySelector('#CARD1').src.replace(/.*png\/./, document.querySelector("#prefSet").textContent)
  }else{
    document.querySelector('#CARD0').src = document.querySelector("#prefDeck").textContent;
    document.querySelector('#CARD1').src = document.querySelector("#prefDeck").textContent;
  };
  for (var i = 0; i < 8; i++) {
    if ( Number(document.querySelector("#prefSet").textContent.match(i)) === i ){
      document.querySelector('#deckPrev1').src = document.querySelector('#prefSet').textContent + 'P.png';
      document.querySelector('#deckPrev2').src = document.querySelector('#prefSet').textContent + 'C.png';
      document.querySelector('#deckPrev3').src = document.querySelector('#prefSet').textContent + 'D.png';
      document.querySelector(".framedS").classList.remove('framedS');
      document.getElementById('deckSetFr' + i).classList.add('framedS');
    };
    if ( Number(document.querySelector("#prefDeck").textContent.match(i)) === i ){
      document.querySelector('#deckPrev0').src = document.querySelector('#prefDeck').textContent;
      document.querySelector(".framed").classList.remove('framed');
      document.getElementById('deckColFr' + i).classList.add('framed');
    };
  };
  if (document.querySelector("#trophyBool").textContent === 'true'){
    document.querySelector("#toggleTrophy").checked = true;
  }else{
  document.querySelector("#toggleTrophy").checked = false;
  };
  if (document.querySelector("#audioBool").textContent === 'true'){
    document.querySelector("#toggleAudio").checked = true;
  }else{
    document.querySelector("#toggleAudio").checked  = false;
  };
  if (document.querySelector("#animationBool").textContent === 'true'){
    document.querySelector("#toggleAnimation").checked = true;
  }else{
  document.querySelector("#toggleAnimation").checked = false;

  };
}
function resetValues()
{
  document.querySelector("#prefBGColor").textContent = document.querySelector("#colorPick").value;
  if ( document.querySelector("#toggleAudio").checked === true ){
    document.querySelector("#audioBool").textContent = 'true';
  }else{
    document.querySelector("#audioBool").textContent = 'false';
  };
  if (document.querySelector("#toggleTrophy").checked === true ){
    document.querySelector("#trophyBool").textContent = 'true';
  }else{
    document.querySelector("#trophyBool").textContent = 'false';
  };
  if (document.querySelector("#toggleAnimation").checked === true ){
    document.querySelector("#animationBool").textContent = 'true';

  }else{
    document.querySelector("#animationBool").textContent = 'false';
  };
  document.querySelector("#prefBoss").textContent = document.querySelector("#bossHide").value;
  document.querySelector("#prefInSum").textContent = document.querySelector("#inSumInp").value;
  document.querySelector("#prefCurrency").textContent = document.querySelector("#currencySelect").value;
  document.querySelector("#prefCurrencyFormat").textContent = document.querySelector("#currencyFormat").value;
}
function about()
{
  document.getElementById("set").classList.add("inactive");
  document.getElementById("aboutDiv").classList.remove("inactive");
  document.getElementById("infoAboutBDiv").classList.remove("inactive");
}
function infoAboutReturn()
{
  document.getElementById("set").classList.remove("inactive");
  document.getElementById("infoDiv").classList.add("inactive");
  document.getElementById("aboutDiv").classList.add("inactive");
  document.getElementById("infoAboutBDiv").classList.add("inactive");
}
function info()
{
  document.getElementById("set").classList.add("inactive");
  document.getElementById("infoDiv").classList.remove("inactive");
  document.getElementById("infoAboutBDiv").classList.remove("inactive");
}
function preferences()
{
  document.getElementById("set").classList.add("prefInactive");
  document.getElementById("preferencePrompt").classList.remove("prefInactive");
}
function YES()
{
  document.querySelector('#h1').textContent = "PRIEST";
  document.getElementById("gamePrompt").classList.add("inactive");
  document.getElementById("yes").classList.add("inactive");
  document.querySelector('#CARD2').classList.remove("tilt");
  document.querySelector('#CARD2').src = document.querySelector("#prefDeck").textContent;
  document.querySelector('#tenHands').classList.remove("inactive");
		document.querySelector('#goDeep').classList.remove("inactive");

  if ( document.querySelector("#animationBool").textContent === 'true' ){
		 document.querySelector('#CARD0').classList.remove("shuffled0");
			document.querySelector('#CARD1').classList.remove("shuffled1");
			document.querySelector('#CARD2').classList.remove("shuffled2");
		}
  selectGame();
}
function trophyCheck(totalSum,sum)
{
  if ( document.querySelector("#trophyBool").textContent === 'true' )
  {
    if ( sum > totalSum / 3 ){trophy('TOP LEVEL HUSTLE!','Aurum omnia vincit!','png/trophies/4.png'); return};
    if ( sum > totalSum / 4){trophy('LUCKYLAND','Population : 1. YOU.','png/trophies/3.png'); return};
    if ( sum > totalSum / 5){trophy('YOUR HOLLINESS!','All kneel before the Archbishop!','png/trophies/5.png');return};
    if ( sum > totalSum / 8){trophy('GREAT WIN!','Whoa, you caught a fat priest there!','png/trophies/1.png');return};
    if ( sum > totalSum / 10){trophy('BOLD MOVE!','Audentes Fortuna iuvat!','png/trophies/0.png');return};
  };
}
function trophy(text0,text1,image)
{
  playSound('wow.mp3');
  document.getElementById("set").classList.add("prefInactive");
  var trophyElems =[{"name": 'trophyDiv',"parent": "setSet","tag": "div"},
    {"name": "trophyP0","parent": "trophyDiv","tag": "p", "text": text0},
    {"name": 'trophyImage',"parent": "trophyDiv","tag": "img", "src": image},
    {"name": "trophyP1","parent": "trophyDiv","tag": "p", "text": text1},
    {"name": 'trophyButtonDiv',"parent": "trophyDiv","tag": "div"},
    {"name": 'trophyDone',"parent": "trophyButtonDiv","tag": "button"}];
  trophyElems.forEach((elem, i) => {
    let parentDiv = "#" + elem.parent;
    let y = elem.name;
    y = document.createElement(elem.tag);
    y.id = elem.name;
    if ( elem.tag === 'p' ) {y.textContent = elem.text};
    if ( elem.tag === 'img' ){y.src = elem.src};
    document.querySelector(parentDiv).appendChild(y);
  });
  trophyDone.textContent = 'Return';
  trophyDone.addEventListener('click', () =>{
    document.getElementById("set").classList.remove("prefInactive");
    document.getElementById("trophyDiv").remove();
  });
}
function playSound(mp3)
{
  audioBool =document.querySelector("#audioBool").textContent;
  if (audioBool === "true"){
    var audio =	document.createElement("audio");
    audio.type	= "audio/wav";
    audio.setAttribute("src", "mp3/"+mp3);
    audio.setAttribute("autoplay", "true");
  }
}
function cardsClosed()
{
  document.querySelector('#CARD0').src = document.querySelector('#prefDeck').textContent;
  document.querySelector('#CARD1').src = document.querySelector('#prefDeck').textContent;
  document.querySelector('#CARD2').src = document.querySelector('#prefDeck').textContent;
}
function inactiveForm()
{
  document.querySelector('#placedBet').classList.add("inactive");
  document.querySelector('#form0').classList.add("inactive");
  document.querySelector('#form1').classList.add("inactive");
}
function logic(pick)
{
  if ( document.querySelector("#animationBool").textContent === 'true' ){
		 document.querySelector('#CARD0').classList.remove("shuffled0");
			document.querySelector('#CARD1').classList.remove("shuffled1");
			document.querySelector('#CARD2').classList.remove("shuffled2");
		}
  let combinations = [ 'PCD', 'PDC', 'CPD', 'DPC', 'CDP', 'DCP'];
  let combo = Math.ceil( Math.random()*(6)) -1 ;
  let openCards = combinations[combo].split('');
  let playCards =['CARD0','CARD1', 'CARD2']
  playCards.forEach((playImage, x) => {
      let y = document.querySelector('#prefSet').textContent + openCards[x] + '.png'
      document.getElementById(playImage).src = y;
  });

  inactiveForm();
  sum = Number(document.querySelector("#placedBet").innerHTML.match(/\d+/g)[0]);
  totalSum = Number(document.querySelector("#total").innerHTML.match(/\d+/g)[0]);
  let coin = document.querySelector('#prefCurrency').textContent;

  if ( String(document.getElementById(pick).src.match("P.png")) === "P.png" ){
    playSound('win.mp3');
    totalSum+=sum;
    if ( document.querySelector('#prefCurrencyFormat').textContent === "true" ){
      document.querySelector('#result2').textContent = "You won " + coin + sum ;
      document.querySelector('#total').textContent = "Total Cash: " + coin + totalSum;
    }else{
      document.querySelector('#result2').textContent = "You won " + sum + coin;
      document.querySelector('#total').textContent = "Total Cash: " + totalSum + coin;
    }
    trophyCheck(totalSum,sum)
  }else{
    playSound('lose.mp3');
    totalSum-=sum;
    if ( document.querySelector('#prefCurrencyFormat').textContent === "true" ){
      document.querySelector('#result2').textContent = "You lost " + coin + sum ;
      document.querySelector('#total').textContent = "Total Cash: " + coin + totalSum ;
    }else{
      document.querySelector('#result2').textContent = "You lost " + sum + coin;
      document.querySelector('#total').textContent = "Total Cash: " + totalSum + coin;
    }
  }
  let hand = document.querySelector("#result1").textContent.replace(/Hand /, '');

  if ( document.querySelector('#prefCurrencyFormat').textContent === "true" ){
    document.querySelector('#placedBet').textContent = coin + '1';
  }else{
    document.querySelector('#placedBet').textContent = '1' + coin;
  }
}
const imageClick0 = function()
{
    document.getElementById("CARD0").removeEventListener('click', imageClick0);
    document.getElementById("CARD1").removeEventListener('click', imageClick1);
    document.getElementById("CARD2").removeEventListener('click', imageClick2);
    document.querySelector('#shuffleButton').classList.remove("inactive");
    inactiveForm();
    logic("CARD0");
    //}

};
const imageClick1 = function()
{
    document.getElementById("CARD0").removeEventListener('click', imageClick0);
    document.getElementById("CARD1").removeEventListener('click', imageClick1);
    document.getElementById("CARD2").removeEventListener('click', imageClick2);
    document.querySelector('#shuffleButton').classList.remove("inactive");
    inactiveForm();
    //}
    logic("CARD1");
};
const imageClick2 = function()
{
    document.getElementById("CARD0").removeEventListener('click', imageClick0);
    document.getElementById("CARD1").removeEventListener('click', imageClick1);
    document.getElementById("CARD2").removeEventListener('click', imageClick2);
    document.querySelector('#shuffleButton').classList.remove("inactive");
    inactiveForm();
    //}
    logic("CARD2");
};
function cardClick()
{
  document.getElementById('CARD0').addEventListener('click', imageClick0);
  document.getElementById('CARD1').addEventListener('click', imageClick1);
  document.getElementById('CARD2').addEventListener('click', imageClick2);
}

function gameStart(phrase)
{
  if ( document.querySelector("#animationBool").textContent === 'true' )
  {
		 document.querySelector('#CARD0').classList.add("shuffled0");
			document.querySelector('#CARD1').classList.add("shuffled1");
			document.querySelector('#CARD2').classList.add("shuffled2");
		}
  let totalSum = document.querySelector("#prefInSum").textContent
  let hand = '0';
  let coin = document.querySelector('#prefCurrency').textContent;
  hand++;
  document.querySelector('#tenHands').classList.add("inactive");
  document.querySelector('#goDeep').classList.add("inactive");
  document.querySelector('#gamePrompt0').classList.add("inactive");
  document.querySelector('#form0').classList.remove("inactive");
  document.querySelector('#form1').classList.remove("inactive");
  document.querySelector('#placedBet').classList.remove("inactive");
  document.querySelector('#form1').max = totalSum;
  if ( document.querySelector('#prefCurrencyFormat').textContent === "true" ){
    document.querySelector('#placedBet').textContent = coin + '1';
  }else{
    document.querySelector('#placedBet').textContent = '1' + coin;
  }
  cardsClosed();
  document.querySelector('#result0').textContent = phrase;
  document.querySelector('#result0').classList.remove("inactive");
  document.querySelector('#result1').classList.remove("inactive");
  document.querySelector('#result2').classList.remove("inactive");
  document.querySelector('#total').classList.remove("inactive");
  if ( document.querySelector('#prefCurrencyFormat').textContent === "true" ){
    document.querySelector('#total').textContent = 'Total Cash: ' + coin + document.querySelector('#prefInSum').textContent;
  }else{
    document.querySelector('#total').textContent = 'Total Cash: ' + document.querySelector('#prefInSum').textContent + coin ;
  }
  document.querySelector('#result1').textContent =  "Hand 1";
  document.querySelector('#result2').textContent =  '';
  cardClick();
}



function selectGame()
{

  inactiveForm();
  document.querySelector('#shuffleButton').classList.add("inactive");
  document.querySelector('#result1').classList.add("inactive");
  document.querySelector('#result2').classList.add("inactive");
  document.querySelector('#total').classList.add("inactive");
  document.querySelector('#gamePrompt0P').textContent = "Select your game:";
}


function gameOver(totalSum,hand,text1,text2,mp3)
{ document.querySelector('#h1').textContent = "GAME OVER";
  playSound(mp3);
  cardsClosed();
  document.querySelector('#CARD2').src = document.querySelector('#prefSet').textContent + 'P.png';
  document.querySelector('#CARD2').classList.add("tilt");
  document.querySelector('#shuffleButton').classList.add("inactive");
  inactiveForm();
  document.querySelector('#result0').classList.add("inactive");
  document.querySelector('#result1').classList.add("inactive");
  document.querySelector('#result2').classList.add("inactive");
  document.querySelector('#total').classList.add("inactive");
  document.querySelector('#gamePrompt0').classList.remove("inactive");
  let coin = document.querySelector('#prefCurrency').textContent;
  if ( document.querySelector('#prefCurrencyFormat').textContent === "true" ){
    document.querySelector('#gamePrompt0P').textContent = text1 + coin + totalSum  + text2 + hand+ " hands. Play again?";
  }else{
    document.querySelector('#gamePrompt0P').textContent = text1 + totalSum + coin + text2 + hand+ " hands. Play again?"
  }

  document.querySelector('#gamePrompt').classList.remove("inactive");
  document.querySelector('#yes').classList.remove("inactive");
}
function nameSum(event)
{
  var y = event.deltaY;
  totalSum = Number(document.querySelector("#total").innerHTML.match(/\d+/g)[0]);
  var sum = Number(document.querySelector("#placedBet").innerHTML.match(/\d+/g)[0]);
  if( y > 0 ){
    if ( sum < totalSum ){
      sum += 1;
      if ( document.querySelector('#prefCurrencyFormat').textContent === 'true' ){
        document.querySelector('#placedBet').textContent = document.querySelector('#prefCurrency').textContent + sum}
      else{ document.getElementById("placedBet").innerHTML = sum + document.querySelector('#prefCurrency').textContent;}
      document.getElementById("form1").value = sum;
    }
  }else{
    if ( sum > 0 ){
      sum -= 1;
      if ( document.querySelector('#prefCurrencyFormat').textContent === 'true' ){
        document.getElementById("placedBet").innerHTML = document.getElementById("prefCurrency").textContent + sum;
      }else{
        document.getElementById("placedBet").innerHTML = sum + document.getElementById("prefCurrency").textContent ;
      }
      document.getElementById("form1").value = sum;
    }
  };
}

function SHUFFLE()
{
	 if ( document.querySelector("#animationBool").textContent === 'true' ){
		 document.querySelector('#CARD0').classList.add("shuffled0");
			document.querySelector('#CARD1').classList.add("shuffled1");
			document.querySelector('#CARD2').classList.add("shuffled2");
		}
  totalSum = Number(document.querySelector("#total").innerHTML.match(/\d+/g)[0]);
  let hand = document.querySelector("#result1").textContent.replace(/Hand /, '');
  hand++;
  let coin = document.querySelector('#prefCurrency').textContent;
  if ( document.querySelector('#prefCurrencyFormat').textContent === "true" ){
    document.querySelector('#total').textContent = "Total Cash: " + coin + totalSum;
  }else{
    document.querySelector('#total').textContent = "Total Cash: " + totalSum + coin;
  }
  document.querySelector('#shuffleButton').classList.add("inactive");
  document.querySelector('#CARD0').src = document.querySelector('#prefDeck').textContent;
  document.querySelector('#CARD1').src = document.querySelector('#prefDeck').textContent;
  document.querySelector('#CARD2').src = document.querySelector('#prefDeck').textContent;
  document.querySelector('#form0').classList.remove("inactive");
  document.querySelector('#form1').classList.remove("inactive");
  document.querySelector('#placedBet').classList.remove("inactive");
  document.querySelector('#form1').max = totalSum;
  document.querySelector('#form1').value = '1';
  document.querySelector('#result1').textContent =  "Hand "+ hand

  if ( document.querySelector('#prefCurrencyFormat').textContent === "true" ){
    document.querySelector('#placedBet').textContent = coin + '1';
  }else{
    document.querySelector('#placedBet').textContent = '1' + coin;
  }
  let gameSelected = document.querySelector('#result0').textContent;
  if(( hand > 10 )&&(gameSelected === "Game: 10 Hands")) {
    hand--;
    if ( totalSum > document.querySelector('#prefInSum').textContent ){
      gameOver(totalSum,hand,"You got away with "," in ",'game_win.mp3');
    }else{gameOver(totalSum,hand,"Well, you managed to go down to "," in ",'game_lose.mp3');}
  }else if( totalSum < 1 ){gameOver(totalSum,hand,"Nice job, you ended up flat broke: "," after ",'game_lose.mp3');
  }else{cardClick();}
}


function keyEvent(event)
{
  var keyDown = event.key;
  if ( keyDown === document.querySelector('#prefBoss').textContent) {
    if ( document.querySelector("#set").classList.contains("bossInactive") === false ){
      document.querySelector("#set").classList.add("bossInactive");
      document.querySelector("#setSet").classList.add("bossInactive");
      document.querySelector("#infoDiv").classList.add("bossInactive");
      document.querySelector("#aboutDiv").classList.add("bossInactive");
      document.querySelector("#infoAboutBDiv").classList.add("bossInactive");
    }else{
      document.querySelector("#set").classList.remove("bossInactive");
      document.querySelector("#setSet").classList.remove("bossInactive");
      document.querySelector("#infoDiv").classList.remove("bossInactive");
      document.querySelector("#aboutDiv").classList.remove("bossInactive");
      document.querySelector("#infoAboutBDiv").classList.remove("bossInactive");
    }
  }
}
/////////////////////////////////////////////build prefered div
var prefered = document.createElement("div");
prefered.id = 'prefered'
prefered.classList.add('inactive');
document.body.appendChild(prefered);
var preferedObjs=[{"name": 'prefBGColor',"textContent": '#2D452F'},
{"name": 'prefSet',"textContent": 'png/3/'},
{"name": 'prefDeck',"textContent": 'png/back/back5.png'},
{"name": 'audioBool',"textContent": 'true'},
{"name": 'trophyBool',"textContent": 'true'},
{"name": 'animationBool',"textContent": 'true'},
{"name": 'prefInSum',"textContent": '100'},
{"name": 'prefCurrency',"textContent": '€'},
{"name": 'prefCurrencyFormat',"textContent": 'true'},
{"name": 'prefAnte',"textContent": '0'},
{"name": 'prefAnteRaise',"textContent": '0'},
{"name": 'prefBoss',"textContent": 'z'}];
preferedObjs.forEach((pref,i) =>{
  let y = pref.name;
  y = document.createElement('p');
  y.id = pref.name;
  y.textContent = pref.textContent;
  document.getElementById("prefered").appendChild(y);
});
cardsClosed();
let coin = document.querySelector('#prefCurrency').textContent;
document.addEventListener('keydown', keyEvent);
document.getElementById("form1").value = '1';
var result0 = document.createElement("p");
result0.id='result0';
document.getElementById("results").appendChild(result0);
var result1 = document.createElement("p");
result1.id='result1';
document.getElementById("results").appendChild(result1);
var total = document.createElement("p");
total.id='total';
document.getElementById("results").appendChild(total);
var result2 = document.createElement("p");
result2.id='result2';
document.getElementById("results").appendChild(result2);
////////////////////////// build prefDivs in setSet
var promptElements = [{"name": 'preferencePrompt',"parent": "setSet","tag": "div"},
  {"name": 'deckPreviewDiv',"parent": "preferencePrompt","tag": "div"},
  {"name": 'deckPrev0',"parent": "deckPreviewDiv","tag": "img", "src": "#prefDeck"},
  {"name": 'deckPrev1',"parent": "deckPreviewDiv","tag": "img", "src": "#prefDeck"},
  {"name": 'deckPrev2',"parent": "deckPreviewDiv","tag": "img", "src": "#prefDeck"},
  {"name": 'deckPrev3',"parent": "deckPreviewDiv","tag": "img", "src": "#prefDeck"},
  {"name": 'prefBackDiv',"parent": "preferencePrompt","tag": "div"},
  {"name": 'bossKeys',"parent": "preferencePrompt","tag": "div"},
  {"name": 'deckSetDiv',"parent": "preferencePrompt","tag": "div"},
  {"name": 'deckColor',"parent": "preferencePrompt","tag": "div"},
  {"name": 'inSumDiv',"parent": "preferencePrompt","tag": "div"},
  {"name": 'currencyDiv',"parent": "preferencePrompt","tag": "div"},
  {"name": 'boolPrompt',"parent": "preferencePrompt","tag": "div"},
  {"name": 'buttonDiv',"parent": "preferencePrompt","tag": "div"},
  {"name": 'colorForm',"parent": "prefBackDiv","tag": "form"},
  {"name": "colorLabel","parent": "colorForm","tag": "p", "text": 'Select Background color:'},
  {"name": 'colorPick',"parent": "colorForm","tag": "input"},
  {"name": 'hideKey',"parent": "bossKeys","tag": "p", "text": 'Hide Key:'},
  {"name": 'bossHide',"parent": "bossKeys","tag": "input"},
  {"name": 'deckSetP',"parent": "deckSetDiv","tag": "p", "text": 'Select Deck:'},
  {"name": 'deckColP',"parent": "deckColor","tag": "p", "text": 'Select Back:'},
  {"name": 'inSumP',"parent": "inSumDiv","tag": "p", "text": 'Initial Sum:'},
  {"name": 'inSumInp',"parent": "inSumDiv","tag": "input"},
  {"name": 'currencyP0',"parent": "currencyDiv","tag": "p", "text": 'Currency:'},
  {"name": 'currencySelect',"parent": "currencyDiv","tag": "select"},
  {"name": 'currencyP1',"parent": "currencyDiv","tag": "p", "text": 'Format:'},
  {"name": 'currencyFormat',"parent": "currencyDiv","tag": "select"},
  {"name": 'trophyLabel',"parent": "boolPrompt","tag": "label", "text": 'Trophies:'},
  {"name": 'toggleTrophy',"parent": "trophyLabel","tag": "input"},
  {"name": 'trophySp',"parent": "trophyLabel","tag": "span"},
  {"name": 'audioLabel',"parent": "boolPrompt","tag": "label", "text": 'Sounds:'},
  {"name": 'toggleAudio',"parent": "audioLabel","tag": "input"},
  {"name": 'audioSp',"parent": "audioLabel","tag": "span"},
  {"name": 'animationLabel',"parent": "boolPrompt","tag": "label", "text": 'Animation:'},
  {"name": 'toggleAnimation',"parent": "animationLabel","tag": "input"},
  {"name": 'animationSp',"parent": "animationLabel","tag": "span"},
  {"name": 'saveSettings',"parent": "buttonDiv","tag": "button"},
  {"name": 'loadSettings',"parent": "buttonDiv","tag": "button"},
  {"name": 'done',"parent": "buttonDiv","tag": "button"}];
promptElements.forEach((elem, i) => {
  let parentDiv = "#" + elem.parent;
  let y = elem.name;
  y = document.createElement(elem.tag);
  y.id = elem.name;
  if (( elem.tag === 'p' )||( elem.tag === 'label' )) {y.textContent = elem.text};
  if ( elem.tag === 'img' ) {let x = document.querySelector(elem.src).textContent; y.src = x};
  document.querySelector(parentDiv).appendChild(y);
});
var cardSets= ['0', '1', '2', '3', '4', '5', '6', '7'];
cardSets.forEach((cardSet,i) =>{
  let parentDiv = 'deckSetFr' + cardSet;
  parentDiv = document.createElement('div');
  parentDiv.id = 'deckSetFr' + cardSet;
  document.getElementById('deckSetDiv').appendChild(parentDiv);
  let y = 'deckSet' + cardSet;
  y = document.createElement('img');
  y.id = 'deckSet' + cardSet;
  y.src = 'png/' + cardSet + '/' + 'P.png';
  document.getElementById('deckSetFr' + cardSet).appendChild(y);
  y.addEventListener('mouseover', () =>{
    deckPrev1.src = 'png/' + cardSet + '/' + 'P.png';
    deckPrev2.src = 'png/' + cardSet + '/' + 'C.png';
    deckPrev3.src = 'png/' + cardSet + '/' + 'D.png';
  });
  y.addEventListener('mouseout', () =>{
    deckPrev1.src = document.querySelector('#prefSet').textContent + 'P.png';
    deckPrev2.src = document.querySelector('#prefSet').textContent + 'C.png';
    deckPrev3.src = document.querySelector('#prefSet').textContent + 'D.png';
  });
  if (document.querySelector("#prefSet").textContent === 'png/' + cardSet + '/' ){
    parentDiv.classList.add('framedS');
  };
  y.addEventListener('click', () =>{
    document.querySelector("#prefSet").textContent = 'png/' + cardSet + '/';
    deckPrev1.src = document.querySelector("#prefSet").textContent +"P.png";
    deckPrev2.src = 'png/' + cardSet + '/' + 'C.png';
    deckPrev3.src = 'png/' + cardSet + '/' + 'D.png';
        if ( String(document.querySelector('#CARD2').src.match('back')) !== "back" ){
      document.querySelector('#CARD2').src = document.querySelector('#CARD2').src.replace(/.*png\/./, 'png/' + cardSet + '/')
    };
    if ( String(document.querySelector('#CARD0').src.match('back')) !== "back" ){
      document.querySelector('#CARD0').src = document.querySelector('#CARD0').src.replace(/.*png\/./, 'png/' + cardSet + '/')
      document.querySelector('#CARD1').src = document.querySelector('#CARD1').src.replace(/.*png\/./, 'png/' + cardSet + '/')
    };
    document.querySelector(".framedS").classList.remove('framedS');
    parentDiv.classList.add('framedS');
  });
});
var backSets = ['0', '1', '2', '3', '4', '5', '6', '7'];
backSets.forEach((backSet,i) =>{
  let parentDiv = 'deckColFr' + backSet;
  parentDiv = document.createElement('div');
  parentDiv.id = 'deckColFr' + backSet;
  document.getElementById('deckColor').appendChild(parentDiv);
  let y = 'deckCol' + backSet;
  y = document.createElement('img');
  y.id = 'deckCol' + backSet;
  y.src = 'png/back/back' + backSet + '.png';
  document.getElementById('deckColFr' + backSet).appendChild(y);
  if (document.querySelector("#prefDeck").textContent.replace(/.*\/png\/back\//, "png/back/") === 'png/back/back' + backSet + '.png' ){
    document.getElementById('deckColFr' + backSet).classList.add('framed');
  }
  y.addEventListener('mouseover', () =>{
    deckPrev0.src = y.src;
  });
  y.addEventListener('mouseout', () =>{
    deckPrev0.src = document.querySelector('#prefDeck').textContent;
  });
  y.addEventListener('click', () =>{
    document.querySelector("#prefDeck").textContent = y.src;
    if ( String(document.querySelector('#CARD2').src.match('back')) === "back" ){
      document.querySelector('#CARD2').src = document.querySelector("#prefDeck").textContent;
    };
    if ( String(document.querySelector('#CARD0').src.match('back')) === "back" ){
      document.querySelector('#CARD0').src = document.querySelector("#prefDeck").textContent;
      document.querySelector('#CARD1').src = document.querySelector("#prefDeck").textContent;
    };
    deckPrev0.src = document.querySelector("#prefDeck").textContent;
    document.querySelector(".framed").classList.remove('framed');
    parentDiv.classList.add('framed');
  });
});
var coins = ['€', '$', '￡', '￥', '₽', '₹','₩', 'L', '₱', 'ƒ', '₿', 'Ξ'];
coins.forEach((coin, i) => {
  let y = coin;
  var z = new Option(y,y);
  currencySelect.appendChild(z);
});
var currrencyF0 = new Option('¤1','true');
var currencyF1 = new Option('1¤','false');
currencyFormat.appendChild(currrencyF0);
currencyFormat.appendChild(currencyF1);
colorPick.type = 'color';
colorPick.name = 'favColor';
bossHide.type = 'text';
bossHide.name = 'boss0';
bossHide.maxLength = "1";
inSumInp.type = 'number';
inSumInp.min = '100';
inSumInp.max = '10000';
inSumInp.step = "100";
toggleTrophy.type = 'checkbox';
toggleAudio.type = 'checkbox';
toggleAnimation.type = 'checkbox';
trophyLabel.classList.add('p');
audioLabel.classList.add('p');
animationLabel.classList.add('p');
trophySp.classList.add('vchkbox');
audioSp.classList.add('vchkbox');
animationSp.classList.add('vchkbox');
reloadPreferedValues();
colorPick.addEventListener('input', () =>{
  resetValues();
  reloadPreferedValues();
});

saveSettings.textContent = "Save";
saveSettings.addEventListener('click', () =>{
  document.getElementById('done').click();
  document.getElementById('prefButton').click();
 var settingsData = {prefBGColor: document.querySelector("#prefBGColor").textContent,
  prefSet: document.querySelector("#prefSet").textContent,
  prefDeck: document.querySelector("#prefDeck").textContent,
  audioBool: document.querySelector("#audioBool").textContent,
  trophyBool: document.querySelector("#trophyBool").textContent,
  animationBool: document.querySelector("#animationBool").textContent,
  prefInSum: document.querySelector("#prefInSum").textContent,
  prefCurrency: document.querySelector("#prefCurrency").textContent,
  prefCurrencyFormat: document.querySelector("#prefCurrencyFormat").textContent,
  prefAnte: document.querySelector("#prefAnte").textContent,
  prefAnteRaise: document.querySelector("#prefAnteRaise").textContent,
  prefBoss: document.querySelector("#prefBoss").textContent};
  var a = document.createElement('a');
  a.id = "a";
  a.href = URL.createObjectURL(new Blob([JSON.stringify(settingsData, null, 2)], {type: "application/json" }));
  buttonDiv.appendChild(a);
  a.download = 'Priest_Settings.json';
  a.click();
  a.remove();
  resetValues();
  document.getElementById("set").classList.remove("prefInactive");
  document.getElementById("preferencePrompt").classList.add("prefInactive");
});
loadSettings.textContent = "Load";
loadSettings.addEventListener('click', () =>{
  var input =  document.createElement('input');
  input.id = "input";
  input.type = 'file';
  input.click();
  input.addEventListener('input', () =>{
    let file = input.files[0];
    let fileReader = new FileReader();
    fileReader.readAsText(file);
    fileReader.onload = function(){
      const elData = JSON.parse(fileReader.result);
      document.querySelector("#prefBGColor").textContent = elData.prefBGColor;
      document.querySelector("#prefSet").textContent = elData.prefSet;
      document.querySelector("#prefDeck").textContent = elData.prefDeck;
      document.querySelector("#audioBool").textContent = elData.audioBool;
      document.querySelector("#trophyBool").textContent = elData.trophyBool;
      document.querySelector("#animationBool").textContent = elData.animationBool;
      document.querySelector("#prefInSum").textContent = elData.prefInSum;
      document.querySelector("#prefCurrency").textContent = elData.prefCurrency;
      document.querySelector("#prefCurrencyFormat").textContent = elData.prefCurrencyFormat;
      document.querySelector("#prefAnte").textContent = elData.prefAnte;
      document.querySelector("#prefAnteRaise").textContent = elData.prefAnteRaise;
      document.querySelector("#prefBoss").textContent = elData.prefBoss;
      reloadPreferedValues();
      resetValues();
    };
    document.getElementById("set").classList.remove("prefInactive");
    document.getElementById("preferencePrompt").classList.add("prefInactive");
  });
});
done.textContent = 'Done';
done.addEventListener('click', () =>{
  resetValues();
  document.getElementById("set").classList.remove("prefInactive");
  document.getElementById("preferencePrompt").classList.add("prefInactive");
});
document.getElementById("preferencePrompt").classList.add("prefInactive");
selectGame();
